import React from 'react';
import Actions from './news-feed.actions';
import Store from './news-feed.store';
import Api from './news-feed.api';

import Post from 'components/post/post';

class NewsFeed extends React.Component {

    constructor() {
        super();
        this.state = this.getNewsFeedState();

        this._onChange = this._onChange.bind(this);
    }

    _onChange() {
        this.setState(this.getNewsFeedState());
    }

    componentWillMount() {
        Store.addChangeListener(this._onChange);

        Api.getMessages().then(({ messages }) => {
            Actions.setMessages(messages);
        });
    }

    componentWillUnmount() {
        Store.removeChangeListener(this._onChange);
    }

    getNewsFeedState() {
        return {
            messages: Store.getMessages()
        };
    }

    onVote(direction, index) {
        Actions.setPostVotes({ direction, index });
    }

    onCommentVote(direction, postIndex, commentIndex) {
        Actions.setPostCommentVotes({ direction, postIndex, commentIndex });
    }

    onAddComment(postId, comment) {
        Actions.addPostComment({ postId, comment });
    }

    onFollow(following, index) {
        Actions.setPostFollowing({ following, index });
    }

    render() {
        return (<ul className="news-feed__list">{
            this.state.messages.map((post, index) =>{
                return (
                    <Post
                        onFollow={this.onFollow}
                        onVote={this.onVote}
                        onCommentVote={this.onCommentVote}
                        onAddComment={this.onAddComment}
                        key={index}
                        postId={index}
                        post={post}
                    />
                )
            })
        }</ul>);
    }
}

export default NewsFeed;