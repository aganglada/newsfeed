import $ from 'jquery';
import config from 'config/api.json';

const METHOD_GET = 'get';
const METHOD_POST = 'post';
const dataType = 'json';

const apiService = () => {

    function request(url, settings) {
        settings = Object.assign(settings, { url });

        return $.ajax(settings);
    }

    function get(url, params) {
        const settings = {
            type: METHOD_GET,
            data: params,
            dataType
        };

        return request(url, settings);
    }

    function post(url, params) {
        const settings = {
            type: METHOD_POST,
            data: params,
            dataType
        };

        return request(url, settings);
    }

    return {
        get, post
    }
};

export default apiService();