import React from 'react';
import moment from 'moment';
import { IntlProvider, FormattedRelative } from 'react-intl';

import Comment from 'components/comment/comment';
import Actions from 'components/actions/actions';
import Topic from 'components/topic/topic';
import CommentForm from 'components/comment-form/comment-form';

class Post extends  React.Component {
    constructor(props) {
        super(props);

        this.follow = this.follow.bind(this);
        this.unfollow = this.unfollow.bind(this);
    }

    unfollow() {
        this.props.onFollow(false, this.props.postId);
    }

    follow() {
        this.props.onFollow(true, this.props.postId);
    }

    renderFollowIcon(following) {
        let button;

        if (following) {
            button = <i onClick={this.unfollow} className="i-svg i-svg-20 i-svg__star-filled" title="Unfollow"/>;
        } else {
            button = <i onClick={this.follow} className="i-svg i-svg-20 i-svg__star-grey" title="Follow"/>;
        }

        return button;
    }

    render() {
        const { post, postId, onVote, onCommentVote, onAddComment } = this.props;

        return (
            <li className="post">
                <figure className="post__author">
                    <img className="post__author-image" src={ post.author.avatar } alt={ post.author.display_name } title={ post.author.display_name } />
                    <strong className="post__author-name">{ post.author.display_name }</strong>
                </figure>
                <h3 className="u-margin-bottom--none">
                    <span className="u-vertical-align--middle u-margin-right--m">{ post.subject }</span>
                    <button type="button" className="e-button u-vertical-align--middle">{
                        this.renderFollowIcon(post.user_is_following)
                    }</button>
                </h3>
                <small className="u-color--grey-light">
                    <IntlProvider locale="en">
                        <FormattedRelative value={post.updated_at}/>
                    </IntlProvider>
                </small>
                <p dangerouslySetInnerHTML={{__html: post.body}}></p>
                {post.topics.map((topic, index) => {
                    return (<Topic topic={topic} key={index} />);
                })}
                <Actions
                    onVote={onVote}
                    postId={postId}
                    size="20"
                    info={post}
                />
                <CommentForm
                    postId={postId}
                    onAddComment={onAddComment}
                />
                <ul>{
                    post.comments.map((comment, index) => {
                        if (
                            index !== post.comments.length -2
                            && index !== post.comments.length -1
                        ) {
                            return false;
                        }

                        return (
                            <Comment
                                onVote={onCommentVote}
                                key={index}
                                commentId={index}
                                postId={postId}
                                comment={comment}
                            />)
                    })
                }</ul>
            </li>
        );
    }
}

export default Post;