import AppDispatcher from 'services/dispacher/dispacher';
import AppStore from 'services/store/store';
import Constants from './news-feed.constants';

const DIRECTION_UP      = '+';
const DIRECTION_DOWN    = '-';

class NewsFeedStore extends AppStore {
    constructor() {
        super();

        this.messages = [];
    }

    getMessages() {
        return this.messages;
    }

    setMessages({ messages }) {
        this.messages = messages;
    }

    setPostFollowing({ following, index }) {
        // fake change
        this.messages[index].user_is_following = following;
    }

    setPostVotes({ direction, index }) {
        // fake change
        if (direction === DIRECTION_UP) {
            this.messages[index].upvotes = this.messages[index].upvotes + 1;
        } else if (direction === DIRECTION_DOWN) {
            this.messages[index].downvotes = this.messages[index].downvotes + 1;
        } else {
            console.error('There is no direction');
        }
    }

    setPostCommentVotes({ direction, postIndex, commentIndex }) {
        // fake change
        if (direction === DIRECTION_UP) {
            this.messages[postIndex].comments[commentIndex].upvotes = this.messages[postIndex].comments[commentIndex].upvotes + 1;
        } else if (direction === DIRECTION_DOWN) {
            this.messages[postIndex].comments[commentIndex].downvotes = this.messages[postIndex].comments[commentIndex].downvotes + 1;
        } else {
            console.error('There is no direction');
        }
    }

    addPostComment({ postId, comment }) {
        this.messages[postId].comments.push({ body: comment });
    }
}

const Store = new NewsFeedStore();

AppDispatcher.register((payload) => {
    let action = payload.action;

    switch (action.type) {
        case Constants.GET_MESSAGES:
            Store.getMessages(action.data);
            break;
        case Constants.SET_MESSAGES:
            Store.setMessages(action.data);
            break;
        case Constants.SET_POST_FOLLOWING:
            Store.setPostFollowing(action.data);
            break;
        case Constants.SET_POST_VOTES:
            Store.setPostVotes(action.data);
            break;
        case Constants.SET_POST_COMMENTS_VOTES:
            Store.setPostCommentVotes(action.data);
            break;
        case Constants.ADD_POST_COMMENT:
            Store.addPostComment(action.data);
            break;
        default:
            return true;
    }

    Store.emitChange();
});

export default Store;