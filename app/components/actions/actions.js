import React from 'react';

class Actions extends  React.Component {
    constructor(props) {
        super(props);

        this.upvote = this.upvote.bind(this);
        this.downvote = this.downvote.bind(this);
    }

    upvote() {
        this.props.onVote('+', this.props.postId, this.props.commentId);
    }

    downvote() {
        this.props.onVote('-', this.props.postId, this.props.commentId);
    }

    render() {
        const { size, info, disabled } = this.props;
        const sizeClassName = `i-svg-${ size ? size : '20' }`;
        const comments = info.comments;

        return (
            <nav className="u-text-align--right u-margin-right--m u-margin-top-bottom--m">
                {
                    info.upvotes || info.upvotes === 0 ?
                        <button onClick={this.upvote} className="e-button" disabled={disabled}>
                            <span className="u-vertical-align--middle u-margin-right--s">{ info.upvotes }</span>
                            <i className={`i-svg ${sizeClassName} i-svg__arrow-up u-vertical-align--middle`} />
                        </button> :
                        null
                }
                {
                    info.downvotes || info.downvotes === 0 ?
                        <button onClick={this.downvote} className="e-button" disabled={disabled}>
                            <span className="u-vertical-align--middle u-margin-right--s">{ info.downvotes }</span>
                            <i className={`i-svg ${sizeClassName} i-svg__arrow-down u-vertical-align--middle`}/>
                        </button> :
                        null
                }
                {
                    info.comments ?
                        <button className="e-button" disabled={disabled}>
                            <span className="u-vertical-align--middle u-margin-right--s">{comments.length ? info.comments.length : 0}</span>
                            <i className={`i-svg ${sizeClassName} i-svg__comment u-vertical-align--middle`} />
                        </button> :
                    null
                }
            </nav>
        );
    }
}

export default Actions;