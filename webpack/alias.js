/*global __dirname*/
var path = require('path');

module.exports = {
    services: path.resolve(
        __dirname,
        '..',
        'app',
        'services'
    ),
    components: path.resolve(
        __dirname,
        '..',
        'app',
        'components'
    ),
    views: path.resolve(
        __dirname,
        '..',
        'app',
        'views'
    ),
    config: path.resolve(
        __dirname,
        '..',
        'config'
    )
};