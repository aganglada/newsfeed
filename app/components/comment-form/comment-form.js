import React from 'react';

import Actions from 'components/actions/actions';

class CommentForm extends  React.Component {
    constructor(props) {
        super(props);

        this.state = {
            body: ''
        };

        this.onSubmit = this.onSubmit.bind(this);
        this.onInputChange = this.onInputChange.bind(this);
    }

    onSubmit(event) {
        event.preventDefault();

        this.props.onAddComment(this.props.postId, this.state.body);

        this.setState({ body: '' });
    }

    onInputChange(event) {
        this.setState({ body: event.target.value });
    }

    render() {
        return (
            <form onSubmit={this.onSubmit}>
                <input
                    type="text"
                    className="e-input"
                    placeholder="Write a comment..."
                    value={this.state.body}
                    onChange={this.onInputChange}
                    required
                />
                <button className="u-display--none" type="submit">Submit</button>
            </form>
        );
    }
}

export default CommentForm;