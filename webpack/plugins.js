var webpack = require('webpack');
var HtmlPlugin = require('html-webpack-plugin');

module.exports =  [
    new HtmlPlugin({
        template: 'index.html'
    }),
    new webpack.NoErrorsPlugin(),
    new webpack.ProvidePlugin({
        'React': 'react'
    }),
    new webpack.DefinePlugin({
        __DEV__: process.env.NODE_ENV !== 'production',
        __PRODUCTION__: process.env.NODE_ENV === 'production',
        'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV)
    })
];