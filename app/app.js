import React from 'react';
import { render } from 'react-dom';
import Header from 'components/header/header';
import NewsFeed from 'views/news-feed/news-feed';
import Footer from 'components/footer/footer';

class App extends React.Component {
    render() {
        return (
            <main>
                <Header></Header>
                <NewsFeed></NewsFeed>
                <Footer></Footer>
            </main>
        );
    }
}

render((
    <App></App>
), document.getElementById('js-app'));

