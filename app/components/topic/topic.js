import React from 'react';

class Topic extends  React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { topic } = this.props;

        return (
            <span className="topic">{ topic.name }</span>
        );
    }
}

export default Topic;