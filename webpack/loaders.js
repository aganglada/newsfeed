/*global __dirname*/
var path = require('path');
var appFolderPath = path.resolve(__dirname, '../app');
var configFolderPath = path.resolve(__dirname, '../config');

module.exports = [
    {
        test: /\.js?$/,
        loaders: [
            'react-hot',
            'babel'
        ],
        include: [
            appFolderPath
        ]
    },
    {
        test: /\.scss$/,
        loader: 'style!css!sass',
        include: [
            appFolderPath
        ]
    },
    {
        test: /\.json/,
        loader: 'json',
        include: [
            appFolderPath,
            configFolderPath
        ]
    }
];