import React from 'react';

class Footer extends React.Component {
    render() {
        return (
            <footer>
                <p className="u-text-align--center">2016 &copy; <strong>Street</strong>life</p>
            </footer>
        )
    }
}

export default Footer;