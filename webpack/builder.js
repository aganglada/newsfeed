var autoprefixer = require('autoprefixer');
var entry = require('./entry');
var output = require('./output');
var loaders = require('./loaders');
var alias = require('./alias');
var plugins = require('./plugins');

module.exports = function(env) {
    return {
        entry: entry,
        output: output,
        devtool: env === 'development' ? 'eval' : 'source-map',
        module: {
            loaders: loaders
        },
        resolve: {
            alias: alias
        },
        postcss: [
            autoprefixer({
                browsers: ['last 2 version']
            })
        ],
        plugins: plugins,
        devServer: {
            historyApiFallback: {
                index: '/build/index.html'
            }
        }
    };
};