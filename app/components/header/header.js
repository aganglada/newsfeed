import React from 'react';

class Header extends React.Component {
    render() {
        return (
            <header className="header u-text-align--center">
                <h1 className="header__logo">
                    <strong>Street</strong>
                    <span>life</span>
                </h1>
            </header>
        )
    }
}

export default Header;