import api from 'services/api/api';
import apiConfig from 'config/api.json';

function NewsFeedApi() {

    function getMessages() {
        return api.get(
            apiConfig.messages
        );
    }

    return {
        getMessages
    };

}

export default NewsFeedApi();