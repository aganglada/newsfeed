# newsfeed

### Running locally

```
$ git clone https://aganglada@bitbucket.org/aganglada/newsfeed.git
$ npm install
$ npm run dev
```

Enter http://localhost:8080 in your browser

Enjoy ;)


### Functionality

* Follow/Unfollow post
* Upvote/Downvote post
* Unvote/Downvote comment
* Add comments to posts