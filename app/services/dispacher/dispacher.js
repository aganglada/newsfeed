import { Dispatcher } from 'flux';

let AppDispatcher = new Dispatcher();

AppDispatcher.handleAction = function(action) {
    AppDispatcher.dispatch({
        source: 'VIEW_ACTION',
        action: action
    });
};

export default AppDispatcher;