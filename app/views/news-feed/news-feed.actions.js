import AppDispatcher from 'services/dispacher/dispacher';
import Constants from './news-feed.constants';

function NewsFeedActions() {

    function getMessages() {
        AppDispatcher.handleAction({
            type: Constants.GET_MESSAGES,
            data: {}
        });
    }

    function setMessages(messages) {
        AppDispatcher.handleAction({
            type: Constants.SET_MESSAGES,
            data: { messages }
        });
    }

    function setPostFollowing(data) {
        AppDispatcher.handleAction({
            type: Constants.SET_POST_FOLLOWING,
            data
        });
    }

    function setPostVotes(data) {
        AppDispatcher.handleAction({
            type: Constants.SET_POST_VOTES,
            data
        });
    }

    function setPostCommentVotes(data) {
        AppDispatcher.handleAction({
            type: Constants.SET_POST_COMMENTS_VOTES,
            data
        });
    }

    function addPostComment(data) {
        AppDispatcher.handleAction({
            type: Constants.ADD_POST_COMMENT,
            data
        });
    }

    return {
        getMessages,
        setMessages,
        setPostFollowing,
        setPostVotes,
        setPostCommentVotes,
        addPostComment
    };
}

export default NewsFeedActions();