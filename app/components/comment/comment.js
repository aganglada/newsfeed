import React from 'react';
import $ from 'jquery';

import Actions from 'components/actions/actions';

import defaultComment from './comment.json';

class Comment extends  React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let { comment, onVote, postId, commentId } = this.props;
        const isDisabled = !comment.body;

        if (!comment.author) {
            comment = $.extend(comment, defaultComment);
        }

        return (
            <li className={`comment ${isDisabled ? 'comment--disabled' : ''}`}>
                <div className="u-clearfix">
                    <figure className="comment__author u-margin--none u-display--inline-block">
                        <img className="comment__author-image" src={ comment.author.avatar } alt=""/>
                    </figure>
                    <p className="comment__body u-margin--none">
                        <strong className="u-display--block u-margin-bottom--s">{ comment.author.display_name }</strong>
                        <span dangerouslySetInnerHTML={{__html: comment.body}}></span>
                    </p>
                </div>
                <Actions
                    onVote={onVote}
                    disabled={isDisabled}
                    postId={postId}
                    commentId={commentId}
                    size="15"
                    info={comment}
                />
            </li>
        );
    }
}

export default Comment;