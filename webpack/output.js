module.exports = {
    path: 'build/',
    filename: '[name].bundle.js',
    publicPath: '/build/'
};